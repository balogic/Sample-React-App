import React from 'react';
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      txt: "State Text",
      count: 0
    }
  }
  update(e) {
    this.setState({txt: (e.target.value)});
  }
  render() {
    return (
      <div>
        <Widget update={this.update.bind(this)} />
        <h2>{this.state.txt}</h2>
      </div>
    )
  }
};
const Widget = (props) => <input type="text" onChange={props.update}/>
export default App;
